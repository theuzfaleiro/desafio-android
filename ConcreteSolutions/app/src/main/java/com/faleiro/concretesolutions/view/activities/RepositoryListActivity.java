package com.faleiro.concretesolutions.view.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.faleiro.concretesolutions.R;
import com.faleiro.concretesolutions.communication.GitHubService;
import com.faleiro.concretesolutions.communication.ServiceFactory;
import com.faleiro.concretesolutions.model.Repository;
import com.faleiro.concretesolutions.model.RepositoryResponse;
import com.faleiro.concretesolutions.util.Constants;
import com.faleiro.concretesolutions.util.EndlessRecyclerViewScrollListener;
import com.faleiro.concretesolutions.util.OnClickListener;
import com.faleiro.concretesolutions.view.adapters.RepositoryAdapter;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class RepositoryListActivity extends AppCompatActivity implements OnClickListener {

    @BindView(R.id.repository_progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.repository_recycler_view)
    RecyclerView repositoryRecyclerView;

    private RepositoryAdapter repositoryAdapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<Repository> repositories = new ArrayList<>();

    private int currentPage = 1;
    private int repositoryCount = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository_list);
        ButterKnife.bind(this);

        setUpRecyclerView();

        addEndlessRecyclerViewScrollListenerToRecyclerView();

        loadRepositoriesFromGitHub();
    }

    private void addEndlessRecyclerViewScrollListenerToRecyclerView() {
        EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (totalItemsCount <= repositoryCount) {
                    currentPage++;

                    showCurrentPageToast();

                    loadRepositoriesFromGitHub();
                }
            }
        };

        repositoryRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
    }

    private void showCurrentPageToast() {
        String infoPageUpdate = getResources().getString(R.string.current_page) + " " + String.valueOf(currentPage);

        Toasty.custom(getApplicationContext(), infoPageUpdate, R.drawable.ic_fork_count, ContextCompat.getColor(getApplicationContext(), R.color.colorIcons), ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), Toast.LENGTH_SHORT, false, true).show();
    }

    private void setUpRecyclerView() {

        layoutManager = new LinearLayoutManager(this);
        repositoryRecyclerView.setLayoutManager(layoutManager);
        repositoryAdapter = new RepositoryAdapter(this, repositories);
        repositoryRecyclerView.setAdapter(repositoryAdapter);
    }

    @Override
    public void onClick(Repository selectedRepository) {
        Intent intent = new Intent(this, PullRequestListActivity.class);
        Bundle extras = new Bundle();
        extras.putSerializable(Repository.BUNDLE_NAME, selectedRepository);
        intent.putExtras(extras);
        startActivity(intent);
    }

    private void loadRepositoriesFromGitHub() {
        AsyncTask<Void, Void, Void> repositoryAsyncTask = new AsyncTask<Void, Void, Void>() {

            private boolean asyncTaskError = false;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                loadRepositoryList();
                return null;
            }

            private void loadRepositoryList() {
                try {
                    repositoryRequest();
                } catch (IOException ex) {
                    asyncTaskError = true;
                }
            }

            private void repositoryRequest() throws IOException {
                GitHubService githubService = ServiceFactory.getServiceFactoryInstance().createGitHubService();
                RepositoryResponse response = githubService.repositoryList(Constants.getLanguageJava(), Constants.getSorting(), currentPage).execute().body();
                repositoryCount = response.getTotalCount();
                repositories.addAll(response.getRepositoryList());
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressBar.setVisibility(View.GONE);
                reloadRecyclerView();
            }

            private void reloadRecyclerView() {
                if (asyncTaskError) {
                    Toasty.error(getApplicationContext(), getResources().getString(R.string.error_while_requesting_the_repositories)).show();
                    onBackPressed();
                } else if (repositories.isEmpty()) {
                    Toasty.warning(getApplicationContext(), getResources().getString(R.string.no_data_was_found_from_this_repository)).show();
                    onBackPressed();
                } else {
                    repositoryAdapter.notifyDataSetChanged();
                }
            }
        };

        repositoryAsyncTask.execute();
    }
}