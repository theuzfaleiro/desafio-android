package com.faleiro.concretesolutions.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Owner implements Serializable {

    private String login;

    @JsonProperty("avatar_url")
    private String avatarURL;

    public String getAvatarURL() {
        return avatarURL;
    }

    public String getLogin() {
        return login;
    }
}