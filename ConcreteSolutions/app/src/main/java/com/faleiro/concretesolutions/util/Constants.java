package com.faleiro.concretesolutions.util;

public class Constants {

    private static final String LANGUAGE_JAVA = "language:java";
    private static final String LANGUAGE_SWIFT = "language:swift";
    private static final String LANGUAGE_JS = "language:js";
    private static final String SORTING = "stars";
    private static final String DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final String BASE_URL = "https://api.github.com/";
    private static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String BUNDLE_NAME = "Pull";

    public static String getLanguageJava() {
        return LANGUAGE_JAVA;
    }

    public static String getLanguageSwift() {
        return LANGUAGE_SWIFT;
    }

    public static String getLanguageJs() {
        return LANGUAGE_JS;
    }

    public static String getSorting() {
        return SORTING;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static String getDatetimeFormat() {
        return DATETIME_FORMAT;
    }

    public static String getDateFormat() {
        return DATE_FORMAT;
    }

    public static String getBundleName() {
        return BUNDLE_NAME;
    }
}
