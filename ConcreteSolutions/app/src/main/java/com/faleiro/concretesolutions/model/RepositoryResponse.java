package com.faleiro.concretesolutions.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class RepositoryResponse {

    @JsonProperty("total_count")
    private int totalCount;

    @JsonProperty("items")
    private List<Repository> repositoryList;

    public int getTotalCount() {
        return totalCount;
    }

    public List<Repository> getRepositoryList() {
        return repositoryList;
    }
}