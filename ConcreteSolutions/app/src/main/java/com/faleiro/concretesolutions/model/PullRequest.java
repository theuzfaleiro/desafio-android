package com.faleiro.concretesolutions.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class PullRequest implements Serializable {


    @JsonProperty("user")
    private Owner owner;

    private String title;

    private String body;

    @JsonProperty("created_at")
    private Date createdAt;

    @JsonProperty("updated_at")
    private Date updatedAt;

    @JsonProperty("html_url")
    private String url;

    public Owner getOwner() {
        return owner;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getUrl() {
        return url;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

}