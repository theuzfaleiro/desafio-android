package com.faleiro.concretesolutions.communication;

import com.faleiro.concretesolutions.model.PullRequest;
import com.faleiro.concretesolutions.model.RepositoryResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface GitHubService {

    @GET("search/repositories")
    Call<RepositoryResponse> repositoryList(@Query("q") String language, @Query("sort") String sort, @Query("page") int page);

    @GET("repos/{user}/{repository}/pulls")
    Call<List<PullRequest>> listPulls(@Path("user") String user, @Path("repository") String repository);
}
