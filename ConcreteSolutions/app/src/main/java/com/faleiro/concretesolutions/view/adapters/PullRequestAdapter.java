package com.faleiro.concretesolutions.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.faleiro.concretesolutions.R;
import com.faleiro.concretesolutions.model.PullRequest;
import com.faleiro.concretesolutions.util.Constants;
import com.faleiro.concretesolutions.view.activities.PullRequestDetailActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.RepositoryViewHolder> {

    private List<PullRequest> pullRequests;
    private LayoutInflater layoutInflater;
    private Context context;
    private DateFormat dateFormat = new SimpleDateFormat(Constants.getDateFormat());


    public PullRequestAdapter(Context context, List<PullRequest> pullRequests) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.pullRequests = pullRequests;
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new RepositoryViewHolder(layoutInflater.inflate(R.layout.pull_request_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder viewHolder, final int position) {
        final PullRequest pull = pullRequests.get(position);

        viewHolder.buttonPullRequestDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PullRequestDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.getBundleName(), pull);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
        viewHolder.setData(pull);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public class RepositoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_view_profile_picture)
        ImageView imageViewProfilePicture;
        @BindView(R.id.text_view_pull_request_name)
        TextView textViewRepositoryName;
        @BindView(R.id.text_view_username)
        TextView textViewRepositoryAuthor;
        @BindView(R.id.text_view_pull_request_body)
        TextView textViewPullRequestBody;
        @BindView(R.id.text_view_created_at)
        TextView textViewCreatedAt;
        @BindView(R.id.text_view_updated_at)
        TextView textViewUpdatedAt;
        @BindView(R.id.button_pull_request_detail)
        Button buttonPullRequestDetail;

        private RepositoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setData(PullRequest pullRequest) {
            setUserProfilePicture(pullRequest);
            textViewRepositoryName.setText(pullRequest.getTitle());
            textViewPullRequestBody.setText(pullRequest.getBody());
            textViewRepositoryAuthor.setText(pullRequest.getOwner().getLogin());
            textViewCreatedAt.setText(dateToString(pullRequest.getCreatedAt()));
            textViewUpdatedAt.setText(dateToString(pullRequest.getUpdatedAt()));
        }

        private void setUserProfilePicture(PullRequest pullRequest) {
            Glide.with(context).load(pullRequest.getOwner().getAvatarURL()).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageViewProfilePicture) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imageViewProfilePicture.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }

    private String dateToString(Date date) {
        return dateFormat.format(date);
    }
}