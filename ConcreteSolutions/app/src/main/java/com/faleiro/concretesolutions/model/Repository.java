package com.faleiro.concretesolutions.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Repository implements Comparable<Repository>, Serializable {

    public static final String BUNDLE_NAME = "REPO";

    private String name;

    private String description;

    @JsonProperty("stargazers_count")
    private int starCount;

    @JsonProperty("forks_count")
    private int forkCount;

    private Owner owner;

    public Owner getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getForkCount() {
        return forkCount;
    }

    public int getStarCount() {
        return starCount;
    }

    @Override
    public int compareTo(Repository repository) {
        return repository.getName().compareTo(name);
    }
}