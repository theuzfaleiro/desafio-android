package com.faleiro.concretesolutions.view.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.faleiro.concretesolutions.R;
import com.faleiro.concretesolutions.communication.GitHubService;
import com.faleiro.concretesolutions.communication.ServiceFactory;
import com.faleiro.concretesolutions.model.PullRequest;
import com.faleiro.concretesolutions.model.Repository;
import com.faleiro.concretesolutions.view.adapters.PullRequestAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class PullRequestListActivity extends AppCompatActivity {

    @BindView(R.id.pull_request_recycler_view)
    RecyclerView pullRequestRecyclerView;

    @BindView(R.id.pull_request_progress_bar)
    ProgressBar pullRequestProgressBar;

    private Repository repository;
    private PullRequestAdapter pullRequestAdapter;
    private List<PullRequest> pullRequest = new ArrayList<>();

    String actionBarTitle = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request_list);

        ButterKnife.bind(this);

        getBundle();

        setUpToolbar();

        setUpRecyclerView();

        loadPullRequestFromGitHub();
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(actionBarTitle);
    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();
        repository = (Repository) bundle.getSerializable(Repository.BUNDLE_NAME);
        actionBarTitle = repository.getName();
    }

    private void setUpRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        pullRequestRecyclerView.setLayoutManager(layoutManager);

        pullRequestAdapter = new PullRequestAdapter(this, pullRequest);
        pullRequestRecyclerView.setAdapter(pullRequestAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void loadPullRequestFromGitHub() {
        AsyncTask<Void, Void, Void> pullRequestAsyncTask = new AsyncTask<Void, Void, Void>() {

            private boolean asyncTaskError = false;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pullRequestProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                loadPullRequests();
                return null;
            }

            private void loadPullRequests() {
                try {
                    pullRequestsRequest();
                } catch (IOException ex) {
                    asyncTaskError = true;
                }
            }

            private void pullRequestsRequest() throws IOException {
                GitHubService gitHubService = ServiceFactory.getServiceFactoryInstance().createGitHubService();
                List<PullRequest> pullRequestResponse = gitHubService.listPulls(repository.getOwner().getLogin(), repository.getName()).execute().body();
                pullRequest.addAll(pullRequestResponse);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                pullRequestProgressBar.setVisibility(View.GONE);
                reloadRecyclerView();
            }

            private void reloadRecyclerView() {
                if (asyncTaskError) {
                    Toasty.error(getApplicationContext(), getResources().getString(R.string.error_while_requesting_the_repositories)).show();
                    onBackPressed();
                } else if (pullRequest.isEmpty()) {
                    Toasty.warning(getApplicationContext(), getResources().getString(R.string.no_data_was_found_from_this_repository)).show();
                    onBackPressed();
                } else {
                    pullRequestAdapter.notifyDataSetChanged();
                }
            }
        };

        pullRequestAsyncTask.execute();
    }
}