package com.faleiro.concretesolutions.util;

import com.faleiro.concretesolutions.model.Repository;

public interface OnClickListener {

    void onClick(Repository repository);
}
