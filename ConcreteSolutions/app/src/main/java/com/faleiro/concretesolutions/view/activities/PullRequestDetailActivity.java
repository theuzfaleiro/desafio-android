package com.faleiro.concretesolutions.view.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.faleiro.concretesolutions.R;
import com.faleiro.concretesolutions.model.PullRequest;
import com.faleiro.concretesolutions.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestDetailActivity extends AppCompatActivity {

    @BindView(R.id.pull_request_detail_web_view)
    WebView pullRequestDetailWebView;

    @BindView(R.id.pull_request_detail_progress_bar)
    ProgressBar pullRequestDetailProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pull_request_detail);

        ButterKnife.bind(this);

        setUpToolbar();

        String pullRequestURL = getBundleURL();

        startWebView(pullRequestURL);
    }

    private void startWebView(String url) {
        pullRequestDetailProgressBar.setVisibility(View.VISIBLE);

        pullRequestDetailWebView.loadUrl(url);
        pullRequestDetailWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pullRequestDetailProgressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getBundleURL() {

        Bundle pullRequestBundle = getIntent().getExtras();
        PullRequest pullRequest = (PullRequest) pullRequestBundle.getSerializable(Constants.getBundleName());

        return pullRequest.getUrl();
    }

    private void setUpToolbar() {
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.pull_requests));
    }
}