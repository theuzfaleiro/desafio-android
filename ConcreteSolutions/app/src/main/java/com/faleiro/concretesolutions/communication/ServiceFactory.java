package com.faleiro.concretesolutions.communication;

import com.faleiro.concretesolutions.util.Constants;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;


public class ServiceFactory {

    private static ServiceFactory serviceFactoryInstance;
    private GitHubService gitHubService = null;

    public static ServiceFactory getServiceFactoryInstance() {
        if (serviceFactoryInstance == null) {
            serviceFactoryInstance = new ServiceFactory();
        }
        return serviceFactoryInstance;
    }

    public GitHubService createGitHubService() {
        if (gitHubService != null) {
            return gitHubService;
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.getBaseUrl())
                .addConverterFactory(JacksonConverterFactory.create(createObjectMapperJackson()))
                .build();

        gitHubService = retrofit.create(GitHubService.class);
        return gitHubService;
    }

    private ObjectMapper createObjectMapperJackson() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        DateFormat df = new SimpleDateFormat(Constants.getDatetimeFormat());
        objectMapper.setDateFormat(df);

        return objectMapper;
    }
}

