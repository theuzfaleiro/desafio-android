package com.faleiro.concretesolutions.view.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.faleiro.concretesolutions.R;
import com.faleiro.concretesolutions.model.Repository;
import com.faleiro.concretesolutions.util.OnClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder> {


    private List<Repository> repositories;
    private LayoutInflater layoutInflater;
    private Context context;
    private OnClickListener repositoryClickListener;

    public RepositoryAdapter(Context context, List<Repository> repositories) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.repositories = repositories;

        if (context instanceof OnClickListener) {
            repositoryClickListener = (OnClickListener) context;
        }
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new RepositoryViewHolder(layoutInflater.inflate(R.layout.repository_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder viewHolder, final int position) {
        final Repository repository = repositories.get(position);
        viewHolder.bindValuesToView(repository);

        viewHolder.buttonPullRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyRepoSelected(repository);
            }
        });
    }

    private void notifyRepoSelected(Repository repository) {
        repositoryClickListener.onClick(repository);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public class RepositoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_view_user)
        ImageView imageViewProfilePicture;
        @BindView(R.id.text_view_repository_name)
        TextView textViewRepositoryName;
        @BindView(R.id.text_view_repository_description)
        TextView textViewRepositoryDescription;
        @BindView(R.id.text_view_owner)
        TextView textViewRepositoryOwner;
        @BindView(R.id.text_view_star_count)
        TextView textViewStarsCount;
        @BindView(R.id.text_view_fork_count)
        TextView textViewForksCount;
        @BindView(R.id.button_repository_detail)
        Button buttonPullRequest;

        private RepositoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindValuesToView(Repository repository) {

            setUserProfilePicture(repository);

            textViewRepositoryName.setText(repository.getName());
            textViewRepositoryDescription.setText(repository.getDescription());
            textViewRepositoryOwner.setText(repository.getOwner().getLogin());
            textViewForksCount.setText(String.valueOf(repository.getForkCount()));
            textViewStarsCount.setText(String.valueOf(repository.getStarCount()));
        }

        private void setUserProfilePicture(Repository repository) {
            Glide.with(context).load(repository.getOwner().getAvatarURL()).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageViewProfilePicture) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imageViewProfilePicture.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }
}