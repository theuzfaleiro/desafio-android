package com.faleiro.concretesolutions;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.faleiro.concretesolutions.view.activities.RepositoryListActivity;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class RepositoryScreenTest {

    @Rule
    public ActivityTestRule<RepositoryListActivity> repositoryListActivityActivityTestRule = new ActivityTestRule<>(RepositoryListActivity.class);

    @Test
    public void goToPullRequestActivity() throws Exception {

        onView(withId(R.id.repository_recycler_view)).perform(
                RecyclerViewActions.actionOnItemAtPosition(1, clickChildViewWithId(R.id.button_repository_detail)));

        onView(withId(R.id.pull_request_recycler_view)).check(matches(isDisplayed()));

        pressBack();

        onView(withId(R.id.repository_recycler_view)).perform(
                RecyclerViewActions.actionOnItemAtPosition(3, clickChildViewWithId(R.id.button_repository_detail)));

        onView(withId(R.id.pull_request_recycler_view)).perform(
                RecyclerViewActions.actionOnItemAtPosition(2, clickChildViewWithId(R.id.button_pull_request_detail)));

        onView(withId(R.id.pull_request_detail_web_view)).check(matches(isDisplayed()));
    }

    public static ViewAction clickChildViewWithId(final int id) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Child ID";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View v = view.findViewById(id);
                v.performClick();
            }
        };
    }
}